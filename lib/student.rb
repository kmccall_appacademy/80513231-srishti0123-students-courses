# class Student
# attr_reader :first_name, :last_name, :courses
#
#   def initialize(first, last)
#     @first_name = first
#     @last_name = last
#     @courses = []
#   end
#
#   def name
#     "#{@first_name} #{@last_name}"
#   end
#
#   def enroll(course)
#     @courses << course unless @courses.include?(course)
#     course.students << self
#   end
#
#   def course_load
#     credit_load = Hash.new(0)
#     @courses.each do |course|
#       credit_load[course.department] += course.credits
#     end
#     credit_load
#   end
# end








# class Student
#   attr_accessor :first_name, :last_name, :courses
#
#   def initialize(first_name, last_name)
#     @first_name = first_name
#     @last_name = last_name
#     @courses = []
#   end
#
#   def name
#     "#{first_name} #{last_name}"
#   end
#
#   def enroll(new_course)
#     b = 0
#     @courses << new_course unless @courses.include?(new_course)
#     new_course.students << self
#
#     if self.courses.any? {|enrolled_course| new_course.conflicts_with?(enrolled_course)}
#       raise "error"
#     end
#   end
#
#   def course_load
#     work_load = Hash.new(0)
#     @courses.each do |course|
#       work_load[course.department] += course.credits
#     end
#     work_load
#   end
#
# end



























class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    work_load = Hash.new(0)
    @courses.each do |course|
      work_load[course.department] += course.credits
    end
    work_load
  end
end
